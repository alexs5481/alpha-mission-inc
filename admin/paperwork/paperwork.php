<?php include '../../view/frontPageHeader.php'; ?>
	
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
      
	<div id="content">
		<h1>Paperwork Test</h1>
		
		<a href="/alpha/files/Setup Pack 2016-2017.pdf" download>Setup Pack 2016-2017</a><br>
		<a href="/alpha/files/W-9 June 2016.pdf" download>W-9 June 2016</a><br>
		<a href="/alpha/files/2016-2017 Insurance.pdf" download>2016-2017 Insurance</a><br>
		<br><br>
		<p><u>Conditional Paperwork</u></p><br>
		<br>
		<a href="/alpha/files/Safety & Disciplinary.pdf" download>Safety & Disciplinary</a><br>
		<a href="/alpha/files/Improvement Plan.pdf" download>Improvement Plan</a><br>
	</div>

	
<?php include '../../view/footer.php'; ?>