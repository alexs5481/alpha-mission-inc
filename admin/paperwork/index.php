<?php 
ob_start();
session_start();

require('../../model/database.php');

if(!$_SESSION['user']['isAdmin'])
{
	header('Location: ../user');
}

$user = $_SESSION['user'];

$action = filter_input(INPUT_POST, 'action');
if ($action === NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action === NULL) {
        $action = 'paperwork'; 
    }
}


if($action == 'paperwork'){
	include('./paperwork.php');
}
ob_end_flush();
?>