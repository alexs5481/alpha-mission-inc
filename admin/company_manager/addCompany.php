<?php include '../../view/frontPageHeader.php';?>
	<div id=content>
	
		<fieldset>
			<legend>Add New Company</legend>
			
			<label>Name:</label>
			<span><?php echo ' ' . $companyName; ?></span><br>
			
			<label>MC Number</label>
			<span><?php echo ' ' . $mc; ?></span><br>
			
			<label>DOT Number</label>
			<span><?php echo ' ' . $dot; ?></span><br>
			
			<label>City:</label>
			<span><?php echo ' ' . $city; ?></span><br>
			<label>HQ State:</label>
			<span><?php echo ' ' . $state; ?></span><br>
			
			<label>Broker's Rating:</label>
			<span><?php echo ' ' . $brokersRating; ?></span><br>
			
			<label>Comments:</label>
			<span><?php echo ' ' . $comments; ?></span><br>
			
			<label>Black List:</label>
			<span><?php echo ' ' . $blacklist; ?></span><br>
			<?php if($blacklist == "Yes"):?>
			<label>Black List Reason:</label>
			<span><?php echo ' ' . $blacklistReason; ?></span><br>
			<?php endif;?>
			
			<form action="." method="post">
				<input type="hidden" name="city" value='<?php echo $city; ?>'/>
				<input type="hidden" name="state" value='<?php echo $state; ?>'/>
				<input type="hidden" name="companyName" value='<?php echo $companyName; ?>'/>
				<input type="hidden" name="mc" value='<?php echo $mc; ?>'/>
				<input type="hidden" name="dot" value='<?php echo $dot; ?>'/>
				<input type="hidden" name="brokersRating" value='<?php echo $brokersRating; ?>'/>
				<input type="hidden" name="comments" value='<?php echo $comments; ?>'/>
				<input type="hidden" name="blacklist" value='<?php echo $blacklist; ?>'/>
				<input type="hidden" name="blacklistReason" value='<?php echo $blacklistReason; ?>'/>
				<input type="hidden" name="action" value="insert_company"/>
			<input type="submit" value="Submit" class=bottomButton />
			</form>
		</fieldset>
	</div>
<?php include '../../view/footer.php'; ?>