<?php include '../../view/frontPageHeader.php'; ?>
	<div id="content">
		<h1>Admin Active Companies</h1>
		
		<a href="?action=company_list">Company List</a>
		<br><br>
		
	        <table border="2">
		        <tr>
		                <th>Name</th>
		                <th>State</th>
		                <th>City</th>
		                <th>MC#</th>
		                <th>DOT#</th>
		                <th>Reason Black Listed</th>
		                <th>&nbsp;</th>
		        </tr>
	           	<?php foreach ($companies as $company) :?>
			<tr style="background-color: #FF6633">			
				<td><?php echo $company['companyName'];?></td>
				<td><?php echo $company['stateName'];?>

				<td><?php echo $company['city'];?></td>
				<td><?php echo $company['mc'];?></td>
				<td><?php echo $company['dot'];?></td>
				<td><?php echo $company['blackListReason'];?></td>
				<td><form action="." method="post">
					<input type="hidden" name="action" value="edit_company">
					<input type="hidden" name="companyID" value="<?php echo $company['companyID']; ?>">
					<input type="submit" value="Edit">
				</form></td>
			</tr>
			<?php endforeach; ?>
	        </table>
    	</div>
	
<?php include '../../view/footer.php'; ?>