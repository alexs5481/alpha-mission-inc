<?php include '../../view/frontPageHeader.php'; ?>
	<div id="content">
		<h1>Admin Active Companies</h1>
		<br>
		<a href="?action=company_list">Company List</a>
		<br><br>
		
	        <table border="2">
		        <tr>
		                <th>Name</th>
		                <th>State</th>
		                <th>City</th>
		                <th>MC#</th>
		                <th>DOT#</th>
		                <th>&nbsp;</th>
		        </tr>
	           	<?php foreach ($companies as $company) :?>
	           	<?php if($company['blackList']): ?>
			<tr style="background-color: #FF6633">				
			<?php elseif($company['preferredList']):?>		
			<tr style="background-color: #00CC66">
			<?php else:?>
			<tr>
			<?php endif;?>
				<td><?php echo $company['companyName'];?></td>
				<td><?php echo $company['stateName'];?>

				<td><?php echo $company['city'];?></td>
				<td><?php echo $company['mc'];?></td>
				<td><?php echo $company['dot'];?></td>
				<td><form action="." method="post">
					<input type="hidden" name="action" value="preferredList">
					<input type="hidden" name="companyID" value="<?php echo $company['companyID']; ?>">
					<input type="submit" value="Preferred List">
				</form></td>
				<td><form action="." method="post">
					<input type="hidden" name="action" value="blackList">
					<input type="hidden" name="companyID" value="<?php echo $company['companyID']; ?>">
					<input type="submit" value="Black List">
				</form></td>
				<td><form action="." method="post">
					<input type="hidden" name="action" value="clearList">
					<input type="hidden" name="companyID" value="<?php echo $company['companyID']; ?>">
					<input type="submit" value="Clear">
				</form></td>
			</tr>
			<?php endforeach; ?>
	        </table>
    	</div>
	
<?php include '../../view/footer.php'; ?>	