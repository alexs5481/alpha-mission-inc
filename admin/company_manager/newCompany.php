<?php include '../../view/frontPageHeader.php'; ?>
	<div id=content>
		<h1> Alpha Mission PRO Board</h1>
		<form action="." method="post">
			<fieldset>
				<legend>New Company Form</legend>
				<input type="hidden" name="action" value="add_company">
				
				<label>Name:</label>
				<input type="text" name="companyName" value='<?php echo $_POST['companyName'];?>'/><br>
				
				<label>MC Number:</label>
				<input type='text' name='mc'></br>
				
				<label>Dot Number:</label>
				<input type='text' name='dot'><br>
				
				<label>HQ City:</label>
				<input type="text" name="city"><br>
				
				
				<label>HQ State:</label>
				<select name="state">
				<?php foreach ($states as $state) :?>
						<option><?php echo $state['stateCode']; ?></option>
				<?php endforeach; ?>
				</select><br> 
				<label>Broker's Rating:</label>
				<select name="brokersRating"> 
					<option disabled selected> - </option>
					<option value="A" <?php if($brokersRating == "A"): ?>selected<?php endif;?>>A</option>
					<option value="B" <?php if($brokersRating == "B"): ?>selected<?php endif;?>>B</option>
					<option value="C" <?php if($brokersRating == "C"): ?>selected<?php endif;?>>C</option>
					<option value="D" <?php if($brokersRating == "D"): ?>selected<?php endif;?>>D</option>
					<option value="F" <?php if($brokersRating == "F"): ?>selected<?php endif;?>>F</option>
					<option value="I" <?php if($brokersRating == "I"): ?>selected<?php endif;?>>I</option>
					<option value="N" <?php if($brokersRating == "N"): ?>selected<?php endif;?>>N</option>
				</select>
				<br>
				<Label>Comments:</label>
				<textarea name="comments" rows="4" cols="50" ></textarea>
				
				<fieldset>
					<legend>Check all that apply</legend>
					<br>
					<input id="blacklist" type="checkbox" name="blacklist" class=checkboxes onClick="blacklisted(blacklistReason)"/>
					Blacklist
					<br>
				</fieldset>
				<div id="blacklistReason"></div>

			</fieldset>
			<input type="submit" value="Submit" class=bottomButton />
			<input type="button" value="Back" onClick="history.go(-1);return true;" class=bottomButton/>
			
		</form>
	</div>
<script>
	function blacklisted(id){
		if(document.getElementById('blacklist').checked){
			id.innerHTML = "<Label>BlackList Reason:</label>"
						+ "<textarea name='blacklistReason' rows='4' cols='50' ></textarea>";
		} else {
			document.getElementById('blacklistReason').innerHTML = "";
		}
	};
</script>
<?php include '../../view/footer.php'; ?>

