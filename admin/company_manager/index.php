<?php
ob_start();
session_start();

require('../../model/database.php');
require('../../model/states_db.php');
require('../../model/company_db.php');

if(!$_SESSION['user']['isAdmin'])
{
	header('Location: ../user');
}

$action = filter_input(INPUT_POST, 'action');
if ($action === NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action === NULL) {
        $action = 'company_list'; 
    }
}

if ($action == 'newCompany_page')
{	
	$states = get_states();
    include('newCompany.php'); 
} 
else if ($action == 'add_company')
{
	$companyName = filter_input(INPUT_POST, 'companyName');
	$city = filter_input(INPUT_POST, 'city');
	$state = filter_input(INPUT_POST, 'state');
	$mc = filter_input(INPUT_POST, 'mc');
	$dot = filter_input(INPUT_POST, 'dot');
	$brokersRating = filter_input(INPUT_POST, 'brokersRating');
	$comments = filter_input(INPUT_POST, 'comments');
	$blacklistReason = filter_input(INPUT_POST, 'blacklistReason');
	
	if(isset($_POST['blacklist'])){
		$blacklist = "Yes";
	}else{
		$blacklist = "No";
	}
	
	include('addCompany.php');
} 
else if ($action=='insert_company')
{
	$city = filter_input(INPUT_POST, 'city');
	$state = filter_input(INPUT_POST, 'state');
	$companyName = filter_input(INPUT_POST, 'companyName');
	$mc = filter_input(INPUT_POST, 'mc');
	$dot = filter_input(INPUT_POST, 'dot');
	$brokersRating = filter_input(INPUT_POST, 'brokersRating');
	$comments = filter_input(INPUT_POST, 'comments');
	$blacklist = filter_input(INPUT_POST, 'blacklist');
	$blacklistReason = filter_input(INPUT_POST, 'blacklistReason');
	
	if($blacklist == "Yes"){
		$blacklist = 1;
	} else {
		$blacklist = 0;
	}
	
	
	if($city == NULL || $state == NULL || $companyName == NULL || $mc == NULL || $brokersRating == NULL)
	{
	
		$error = "You must fill out all fields.";
		include('../../errors/error.php');
	}
	else 
	{
        add_company($city, $state, $companyName, $mc, $dot, $brokersRating, $comments, $blacklist, $blacklistReason);	 
        header("Location: .");
	}
} else if ($action == 'company_list'){

	$companies = get_companies();
	include('company_list.php');	
	
} else if ($action == 'black_list'){

	$companies = get_black_list_companies();
	include('black_list.php');	
	
} else if ($action == 'edit_company_list'){
	
	$companies = get_companies();
	include('editList.php');

} else if ($action == 'preferredList'){

	$companyID = filter_input(INPUT_POST, 'companyID');
	set_preferred($companyID);
	header('Location: .');

} else if ($action == 'blackList'){  

	$companyID = filter_input(INPUT_POST, 'companyID');
	set_blackList($companyID);
	header('Location: .');

} else if ($action == 'clearList'){  

	$companyID = filter_input(INPUT_POST, 'companyID');
	clear_lists($companyID);
	header('Location: .');
}
else if ($action == 'edit_company')
{
	$companyID = filter_input(INPUT_POST, 'companyID');
	$company = get_company_by_id($companyID);
	$brokersRating = $company['latestRating'];
	$states = get_states();
	include('./editCompany.php');
}
else if ($action == 'submit_edit_company')
{
	$companyID = filter_input(INPUT_POST, 'companyID');
	$companyName= filter_input(INPUT_POST, 'companyName');
	$mc= filter_input(INPUT_POST, 'mc');
	$dot= filter_input(INPUT_POST, 'dot');
	$city= filter_input(INPUT_POST, 'city');
	$state= filter_input(INPUT_POST, 'state');
	$reasonBlackListed = filter_input(INPUT_POST, 'reasonBlackListed');
	$comments = filter_input(INPUT_POST, 'comments');
	$latestRating = filter_input(INPUT_POST, 'brokersRating');
	update_company($companyID, $companyName, $mc, $dot, $city, $state, $reasonBlackListed, $comments, $latestRating);
	header('Location: .');
} 
else if ($action == 'filter')
{
	
	$companyName= filter_input(INPUT_POST, 'company');
	if($companyName != null)
		$companies = get_companies_by_like_name($companyName);
	else 
		$companies = get_companies();
	include('company_list.php');
}
ob_end_flush();
?>