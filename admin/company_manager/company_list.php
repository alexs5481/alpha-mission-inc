<?php include '../../view/frontPageHeader.php'; ?>
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

	<div id="content">
		<h1>Admin Active Companies</h1>
		<br>
		<a href="?action=newCompany_page">New Company</a>
		<br>
		<a href="?action=black_list">Black List</a>
		<br>
		<a href="?action=edit_company_list">Edit List</a>
		<br><br>
		
		<script>
	  	$(function() {
	    		$( "#company" ).autocomplete({
	      		source: '../../model/searchCompanies.php'
	    		});
	  	});
        </script>
		
		<form action='.' method='post'>
			<input type='hidden' name='action' value='filter'>
			<label for="company">Companies: </label>
			<input id="company" name='company'>
			<br>
			
			<input type="submit" value="Submit" />
			<br>
		</form>
		
		<table border="2">
			<tr>
					<th>Name</th>
					<th>State</th>
					<th>City</th>
					<th>MC#</th>
					<th>DOT#</th>
					<th>Rating</th>
					<th>Comments</th>
					<th>Reason Blacklisted</th>
					<th>&nbsp;</th>
			</tr>
			<?php 
				foreach ($companies as $company) :
			?>
			<?php if($company['blackList']): ?>
		<tr style="background-color: #FF6633">				
		<?php elseif($company['preferredList']):?>		
		<tr style="background-color: #00CC66">
		<?php else:?>
		<tr>
		<?php endif;?>
			<td><?php echo $company['companyName'];?></td>
			<td><?php echo $company['stateName'];?>

			<td><?php echo $company['city'];?></td>
			<td><?php echo $company['mc'];?></td>
			<td><?php echo $company['dot'];?></td>
			<td><?php echo $company['latestRating'];?></td>
			<td><?php echo $company['comments'];?></td>
			<td><?php echo $company['blackListReason'];?></td>
			<td><form action="." method="post">
				<input type="hidden" name="action" value="edit_company">
				<input type="hidden" name="companyID" value="<?php echo $company['companyID']; ?>">
				<input type="submit" value="Edit">
			</form></td>
		</tr>
		<?php 
			endforeach; ?>
		</table>

	</div>
	
<?php include '../../view/footer.php'; ?>	