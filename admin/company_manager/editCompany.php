<?php include '../../view/frontPageHeader.php'; ?>
	<div id=content>
		<h1> Alpha Mission PRO Board</h1>
		<form action="." method="post">
			<fieldset>
				<legend>Edit Company Form</legend>
				<p>Company being edited: <?php echo $company['companyName']; ?></p><br><br>
				<input type="hidden" name="action" value="submit_edit_company" >
				<input type='hidden' name='companyID' value='<?php echo $company['companyID'];?>'>
				
				<label>Name:</label>
				<input type="text" name="companyName" value='<?php echo $company['companyName'];?>'/><br>
				
				<label>MC Number:</label>
				<input type='text' name='mc' value='<?php echo $company['mc'];?>'></br>
				
				<label>Dot Number:</label>
				<input type='text' name='dot' value='<?php echo $company['dot'];?>'><br>
				
				<label>HQ City:</label>
				<input type="text" name="city" value='<?php echo $company['city'];?>'><br>
				
				
				<label>HQ State:</label>
				<select name="state">
					<?php foreach ($states as $state) :?>
							<option <?php if($company['stateName'] == $state['stateCode']):?> selected <?php endif;?>>
							<?php echo $state['stateCode'];?></option>
					<?php endforeach; ?>
				</select>
				<br>
				<label>Latest Rating:</label>
				<select name="brokersRating" >
				
					<?php if($brokersRating == Null):?> 
					<option disabled selected> - </option>
					<?php endif; ?> 
					
					<option value="A" <?php if($brokersRating == "A"): ?>selected<?php endif;?>>A</option>
					<option value="B" <?php if($brokersRating == "B"): ?>selected<?php endif;?>>B</option>
					<option value="C" <?php if($brokersRating == "C"): ?>selected<?php endif;?>>C</option>
					<option value="D" <?php if($brokersRating == "D"): ?>selected<?php endif;?>>D</option>
					<option value="F" <?php if($brokersRating == "F"): ?>selected<?php endif;?>>F</option>
					<option value="I" <?php if($brokersRating == "I"): ?>selected<?php endif;?>>I</option>
					<option value="N" <?php if($brokersRating == "N"): ?>selected<?php endif;?>>N</option>
				</select>
				<br>
				<label>Comments:</label>
				<textarea name="comments" rows="4" cols="50"><?php echo $company['comments'];?></textarea>
				
				<?php if($company['blackList'] == 1):?>
				<br>
				<label>Reason Black Listed:</label>
				<textarea name="reasonBlackListed" rows="4" cols="50"><?php echo $company['blackListReason'];?></textarea><br>
				<?php endif;?>
				</fieldset>
			<input type="submit" value="Submit" class=bottomButton />
			<input type="button" value="Back" onClick="history.go(-1);return true;" class=bottomButton/>
		</form>
	</div>
<?php include '../../view/footer.php'; ?>