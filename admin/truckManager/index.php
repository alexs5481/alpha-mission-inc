<?php
ob_start();
session_start();

require('../../model/database.php');
require('../../model/states_db.php');
require('../../model/driver_db.php');
require('../../model/trucks_db.php');

$action = filter_input(INPUT_POST, 'action');

if(!$_SESSION['user']['isAdmin'])
{
	header('Location: ../user');
}

if ($action === NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action === NULL) {
        $action = 'truckList'; 
    }
}


if($action == 'truckList'){
	$trucks = getActiveTrucks();
	include('truckList.php');
	
} else if($action == 'markAsInactive'){
	$id = filter_input(INPUT_POST, 'id');
	setTruckInactive($id);
	header('Location: .');
	
} else if($action == 'inactiveList'){
	$trucks = getInActiveTrucks();
	include('inactiveTruckList.php');
	
} else if($action == 'markAsActive'){
	$id = filter_input(INPUT_POST, 'id');
	setTruckActive($id);
	header('Location: .');

} else if($action == 'newTruck'){
	include('newTruck.php');
	
} else if($action == 'addTruck'){
	$trNumber = filter_input(INPUT_POST, 'trNumber');
	addTruck($trNumber);
	header('Location: .');
	
}

ob_end_flush();
?>