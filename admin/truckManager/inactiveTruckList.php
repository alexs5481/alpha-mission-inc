<?php include '../../view/frontPageHeader.php'; ?>
	
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
      
	<div id="content">
		<h1>InActive Trucks</h1>
		
		<a href="?action=truckList">Active Truck List</a><br><br>
		
	        <table border="2">
	            <tr>
	                <th>Truck#</th>
	                <th>&nbsp;</th>
	            </tr>
	            <?php foreach ($trucks as $truck) :?>
				
				<tr>			
					<td><?php echo $truck['id'];?></td>
					<td><form action="" method="post">
						<input type="hidden" name="action" value="markAsActive">
						<input type="hidden" name="id" value=<?php echo $truck['id']?>>
						<input type="submit" value="Set Active">
					</form></td>
				</tr>
				
				<?php endforeach; ?>
	        </table>
    	</div>
	
<?php include '../../view/footer.php'; ?>