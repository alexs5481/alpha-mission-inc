<?php 
ob_start();
session_start();

require('../../model/database.php');
require('../../model/states_db.php');
require('../../model/company_db.php');
require('../../model/driver_db.php');
require('../../model/pro_db.php');
require('../../model/brokerRating_db.php');
require('../../model/trucks_db.php');
require('../../model/automatedUpdatesFunctions.php');

if(!$_SESSION['user']['isAdmin'])
{
	header('Location: ../user');
}

$user = $_SESSION['user'];
$action = filter_input(INPUT_POST, 'action');
if ($action === NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action === NULL) {
        $action = 'admin_page'; 
    }
}

if($action == 'admin_page'){
	include('./automatedUpdates.php');

} 
ob_end_flush();
?>