<?php 
ob_start();
session_start();

require('../../model/database.php');
require('../../model/comments_db.php');

if(!$_SESSION['user']['isAdmin'])
{
	header('Location: ../user');
}

$user = $_SESSION['user'];

$action = filter_input(INPUT_POST, 'action');
if ($action === NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action === NULL) {
        $action = 'notFinishedComments'; 
    }
}


if($action == 'notFinishedComments'){
	$comments = getNotFinishedComments();
	include('./comments_list.php');
	
} else if($action == 'markAsCompleted'){
	$id = filter_input(INPUT_POST, 'id');
	commentCompleted($id);
	header('Location: .');
	
}else if($action == 'newComment'){
	include('./addComments.php');
	
}else if($action == 'addComment'){
	$comment = filter_input(INPUT_POST, 'comments');
	$priority = filter_input(INPUT_POST, 'priority');
	add_comment($comment, $priority);
	header('Location: .');
	
}else if($action == 'finished'){
	$comments = getFinishedComments();
	include('./finishedComments.php');
	
} else if($action == 'markAsNotCompleted'){
	$id = filter_input(INPUT_POST, 'id');
	commentNotCompleted($id);
	header('Location: .');
	
} else if($action == 'edit'){
	$id = filter_input(INPUT_POST, 'id');
	$comment = getComment($id);
	include('./editComment.php');
	
} else if($action == 'submitEdit'){
	$id = filter_input(INPUT_POST, 'id');
	$comment = filter_input(INPUT_POST, 'comments');
	$priority = filter_input(INPUT_POST, 'priority');
	updateComment($id, $comment, $priority);
	header('Location: .');
	
}
ob_end_flush();
?>