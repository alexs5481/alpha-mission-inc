<?php include '../../view/frontPageHeader.php'; ?>
	
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
      
	<div id="content">
		<h1>Comments</h1>

		<a href="?action=newComment">Add Comment</a><br>
		<a href="?action=finished">Finished Comments</a><br><br>
		
	        <table border="2">
	            <tr>
	                <th>Comment</th>
					<th>Priority</th>
	                <th>&nbsp;</th>
					<th>&nbsp;</th>
	            </tr>
	            <?php foreach ($comments as $comment) :?>
				
				<tr>			
					<td><?php echo $comment['comments'];?></td>
					<td><?php echo $comment['priority'];?></td>
					<td><form action="." method="post">
						<input type="hidden" name="action" value="edit">
						<input type="hidden" name="id" value=<?php echo $comment['id']?>>
						<input type="submit" value="Edit">
					</form></td>
					<td><form action="." method="post">
						<input type="hidden" name="action" value="markAsCompleted">
						<input type="hidden" name="id" value=<?php echo $comment['id']?>>
						<input type="submit" value="Done">
					</form></td>
					
				</tr>
				
				<?php endforeach; ?>
	        </table>
    	</div>
	
<?php include '../../view/footer.php'; ?>