<?php 
ob_start();
session_start();



if(!$_SESSION['user']['isAdmin'])
{
	header('Location: ../user');
}

$user = $_SESSION['user'];
$action = filter_input(INPUT_POST, 'action');
if ($action === NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action === NULL) {
        $action = 'admin_page'; 
    }
}

if($action == 'admin_page'){
	include('./home.php');

} 
ob_end_flush();
?>