<?php include '../../../view/frontPageHeader.php'; ?>
	
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
      
	<div id="content">
	
		<form action="." method="post">
			<label>Truck Number: </label>
				<select name="trNumber">
					<?php foreach ($drivers as $driver) :
							if($driver['truckNumber'] != "0000"):
					?>
							<option><?php echo $driver['firstName'] . " " . $driver['lastName']; ?></option>
							
					<?php endif;
					endforeach; ?>
				</select>
			<br>
			<br>
			<label>Total Miles:</label>
			<input type="text" name="miles" class="textbox"><br>
			<br>
			<label>Receipts:</label>
			<input type="text" name="receipts" class="textbox"><br>
			<br>
			<div id="advanceArea">
			</div>
			<div id="removeAdvance">
			</div>
			<div id="advancedCount">
			</div>
			<input type="button" value="Add Advance" onClick="addAdvance(advanceArea)" /><br>
			<input type="button" value="update" onClick="update(advancedCount)" /><br>
			<br>
			<input type="submit" value="Submit" />
			<input type="button" value="Back" onClick="history.go(-1);return true;" class=bottomButton/><br>
		</form>

	</div>
<script>
	var advanceCount = 1;
	var lastAdvance = '';
	function addAdvance(id){
		id.innerHTML = id.innerHTML + '<div id="advanceArea' + advanceCount + '">'
									+ '<label>Advance ' + advanceCount + ':</label><br>'
									+ '<br><br>'
									+ '</div>';
		document.getElementById('removeAdvance').innerHTML = '<input type="button" value="Remove Advance" onClick="removeAdvance()"/><br>';
		lastAdvance= 'advanceArea' + advanceCount;
		advanceCount ++;
		
		document.getElementById('advancedCount').innerHTML = lastAdvance;
	};
	function removeAdvance(){
		document.getElementById(lastAdvance).innerHTML = '';
		
		if(advanceCount > 1){
			advanceCount = advanceCount-1;
		} 
		if(advanceCount == 1){
			document.getElementById('removeAdvance').innerHTML = '';
			document.getElementById('advanceArea').innerHTML = '';
		}
		document.getElementById('advancedCount').innerHTML = lastAdvance;
		if(document.getElementById(lastAdvance).innerHTML == ''){
			document.getElementById(("advanceArea" + advanceCount)).innerHTML = '';
		}
			/*document.getElementById('display').innerHTML;*/
	};
	function update(id){
		id.innerHTML = advanceCount;
	};
</script>
	
<?php include '../../../view/footer.php'; ?>