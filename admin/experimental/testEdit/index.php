<?php 
ob_start();
session_start();

require('../../../model/database.php');
require('../../../model/pro_db.php');

if(!$_SESSION['user']['isAdmin'])
{
	header('Location: ../user');
}

$user = $_SESSION['user'];

$action = filter_input(INPUT_POST, 'action');
if ($action === NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action === NULL) {
        $action = 'test'; 
    }
}


if($action == 'test'){
	$load = get_load_by_id(16001);
	$test = $load['proNumber'];
	include('./test.php');
	
}
ob_end_flush();
?>