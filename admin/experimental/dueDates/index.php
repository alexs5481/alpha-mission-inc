<?php 
ob_start();
session_start();

require('../../../model/database.php');
require('../../../model/dues_db.php');

if(!$_SESSION['user']['isAdmin'])
{
	header('Location: ../user');
}

$user = $_SESSION['user'];

$action = filter_input(INPUT_POST, 'action');
if ($action === NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action === NULL) {
        $action = 'test'; 
    }
}


if($action == 'test'){
	$dues = getDues();
	include('./test.php');
	
}
ob_end_flush();
?>