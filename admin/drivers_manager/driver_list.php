<?php include '../../view/frontPageHeader.php'; ?>
	<div id="content">
		<h1>Admin Driver Information</h1>
		<a href="./?action=add_driver">Add Drivers</a> 
		<br>
		<?php if($action == terminatedDrivers):?>
			<a href="./?action=drivers_list">Active Drivers</a> 
		<?php else:?>
			<a href="./?action=terminatedDrivers">Terminated Drivers</a> 
		<?php endif;?>
		<br>
		<br>
		
	        <table border="2">
	            <tr>
	                <th>Truck #</th>
	                <th>Name</th>
	                <th>Birthday</th>
	                <th>Phone Number</th>
	                <th>isActive</th>
	                <th>Date Added</th>
	                <th>Termination Date</th>
	            </tr>
	           <?php foreach ($drivers as $driver) :?>
			<tr>						
				<td><?php echo $driver['truckNumber'];?></td>
				<td><?php echo $driver['firstName'] . ' ' . $driver['lastName'];?></td>
				<td><?php echo $driver['birthday'];?></td>
				<td><?php echo $driver['phoneNum'];?></td>
				<td><?php if($driver['isActive'])echo 'Yes';
					  else echo 'No'; ?></td>
				<td><?php echo $driver['dateAdded'];?></td>
				<td><?php echo $driver['terminationDate'];?></td>
				
				<td><form action="." method="post">
					<input type="hidden" name="action"
						   value="edit_driver">
					<input type="hidden" name="driverId"
						   value="<?php echo $driver['id']; ?>">
					<input type="submit" value="Edit">
				</form></td>
				
				<!--- Doesn't Do anything yet maybe move it into edit driver page.
				<?php if($driver['isActive']): ?>
				<td><form action="." method="post">
					<input type="hidden" name="action"
						   value="terminate_driver">
					<input type="hidden" name="driverId"
						   value="<?php echo $driver['id']; ?>">
					<input type="submit" value="Terminate">
				</form></td><br>
				<?php else:?>
				<td><form action="." method="post">
					<input type="hidden" name="action"
						   value="reinstate_driver">
					<input type="hidden" name="driverId"
						   value="<?php echo $driver['id']; ?>">
					<input type="submit" value="Reinstate">
				</form></td><br>
				<?php endif; ?>---->
			</tr>
			<?php endforeach; ?>
	        </table>
    	</div>
	
<?php include '../../view/footer.php'; ?>