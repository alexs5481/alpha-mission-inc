<?php
ob_start();
session_start();

require('../../model/database.php');
require('../../model/states_db.php');
require('../../model/driver_db.php');

$driverID = filter_input(INPUT_POST, 'driverId');
$action = filter_input(INPUT_POST, 'action');

if(!$_SESSION['user']['isAdmin'])
{
	header('Location: ../user');
}

if ($action === NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action === NULL) {
        $action = 'drivers_list'; 
    }
}

if ($action == 'add_driver'){
    include('newDrivers.php'); 
} 
else if($action == 'display_info')
{
	$firstName = filter_input(INPUT_POST, 'firstName');
	$lastName = filter_input(INPUT_POST, 'lastName');
	$trNumber = filter_input(INPUT_POST, 'trNumber');
	$birthday = filter_input(INPUT_POST, 'birthday');
	$phone = filter_input(INPUT_POST, 'phone');
	include('addDriver.php');
}
else if($action == 'insert_driver')
{
	
	$firstName = filter_input(INPUT_POST, 'firstName');
	$lastName = filter_input(INPUT_POST, 'lastName');
	$trNumber = filter_input(INPUT_POST, 'trNumber');
	$birthday = filter_input(INPUT_POST, 'birthday');
	$phone = filter_input(INPUT_POST, 'phone');
	$dateAdded = date("Y-m-d");
	
	if($firstName == NULL || $lastName == NULL || $trNumber == NULL)
	{
		$error = "You must fill out all fields.";
		include('../errors/error.php');
	}
	else 
	{
        add_driver($firstName, $lastName, $trNumber, $birthday, $dateAdded, $phone);	 
        header("Location: ./index.php");
	}
} else if($action == 'drivers_list'){

	$drivers = getActiveDrivers();
	include('driver_list.php');

} else if($action == 'edit_driver'){
	
	$driver = get_driver_by_id($driverID);
	include('./driverEdit.php');
	
} else if($action == 'update_driver'){

	$firstName = filter_input(INPUT_POST, 'firstName');
	$lastName = filter_input(INPUT_POST, 'lastName');
	$truckNumber= filter_input(INPUT_POST, 'trNumber');
	$birthday = filter_input(INPUT_POST, 'birthday');
	$phoneNum= filter_input(INPUT_POST, 'phoneNum');

	update_driver($driverID, $truckNumber, $firstName, $lastName, $birthday, $phoneNum);
	header('Location: .');
	
}else if($action == 'terminate_driver'){

	$date = date("m/d/Y");
	$driverID = filter_input(INPUT_POST, 'driverId');
	$driver = get_driver_by_id($driverID);
	include('./terminateDriver.php');

} else if($action == 'terminate'){
	$driverID = filter_input(INPUT_POST, 'driverId');
	$term= filter_input(INPUT_POST, 'term');
	if($term){
		$date = date("Y-m-d");
		terminate_driver($driverID, $date);
	}
	header('Location: .');
	
} else if($action == 'reinstate_driver'){

	reinstate_driver($driverID);
	header('Location: .');
} else if($action == 'terminatedDrivers'){
	$drivers = getTerminatedDrivers();
	include('driver_list.php');
}

ob_end_flush();
?>