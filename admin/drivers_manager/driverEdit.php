<?php include '../../view/frontPageHeader.php'; ?>
	<div id=content>
		<fieldset>
			<legend>Driver Options</legend>
			<form action='.' method="post">
				<input type="hidden" name="action" value = "terminate_driver">
				<input type='hidden' name='driverId' value='<?php echo $driver['id'];?>'>
				<input type="submit" value="Terminate Driver" /><br>
			</form>
			<form action='.' method="post">
				<input type="hidden" name="action" value = "reinstate_driver">
				<input type='hidden' name='driverId' value='<?php echo $driver['id'];?>'>
				<input type="submit" value="Reinstate Driver" /><br>
			</form>
		</fieldset>
		<form action='.' method="post">
			<fieldset>
				<p>Driver:<?php echo ' ' . $driver['firstName'] . ' ' . $driver['lastName'];?></p>
				<input type="hidden" name='action' value="update_driver">
				<input type='hidden' name='driverId' value='<?php echo $driver['id'];?>'>
				<legend>New Driver Form</legend>
				<label>First Name:</label>
				<input type="text" name="firstName" class=textbox value='<?php echo $driver['firstName']; ?>'><br>
				<label>Last Name:</label>
				<input type="text" name="lastName" class=textbox value='<?php echo $driver['lastName']; ?>'><br>
				<label>Truck Number:</label>
				<input type="text" name="trNumber" class=textbox value='<?php echo $driver['truckNumber'];?>'><br>
				<label>Birthday:</label>
				<input type="date" name="birthday" value='<?php echo $driver['birthday']; ?>' >
				<label>(mm/dd/yyyy)</label><br>
				<label>Phone Number:</label>
				<input type="text" name="phoneNum" value='<?php echo $driver['phoneNum']; ?>' ><br>
			</fieldset>
			<input type="submit" value="Submit" />
			<input type="button" value="Back" onClick="history.go(-1);return true;" class=bottomButton/><br>
		</form>
	</div>
<?php include '../../view/footer.php'; ?>