<?php include '../../view/frontPageHeader.php'; ?>
	<div id=content>
		<form action='.' method="post">
			<fieldset>
				<input type="hidden" name='action' value="display_info">
				<legend>New Driver Form</legend>
				<label>First Name:</label>
				<input type="text" name="firstName" class=textbox><br>
				<label>Last Name:</label>
				<input type="text" name="lastName" class=textbox><br>
				<label>Truck Number:</label>
				<input type="text" name="trNumber" class=textbox><br>
				<label>Phone Number:</label>
				<input type="text" name="phone" class=textbox><br>
				<label>Birthday:</label>
				<input type="date" name="birthday" >
				<label>(mm/dd/yyy)</label>
			</fieldset>
			<input type="submit" value="Submit" />
			<input type="button" value="Back" onClick="history.go(-1);return true;" class=bottomButton/>
		</form>
	</div>
<?php include '../../view/footer.php'; ?>