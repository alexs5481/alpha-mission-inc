<?php include '../../view/frontPageHeader.php'; ?>
		<div id=content>
			<h1> Alpha Mission PRO Board</h1>
			<fieldset>
			
				<legend>Add New PRO?</legend>
				<label>PRO:</label>
				<span><?php echo ' ' . $pro; ?></span><br>
				
				<label>Company:</label>
				<span><?php echo ' ' . $company['companyName']; ?></span><br>
				
				<label>Truck:</label>
				<span><?php echo ' ' . $trNumber; ?></span><br>
				
				<label>Amount:</label>
				<span><?php echo ' $' . $amount; ?></span><br>
				
				<label>Drivers Pay:</label>
				<span><?php echo ' $' . $driverPay; ?></span><br>
				
				<label>Brokers Name:</label>
				<span><?php echo ' ' . $broker; ?></span><br>
				
				<label>Brokers Rating:</label>
				<span><?php echo ' ' . $brokersRating; ?></span><br>
				
				<label>Date:</label>
				<span><?php echo ' ' . $date; ?></span><br>
				
				<label>Is this a TONU?</label>
				<span><?php echo ' ' . $tonu; ?></span><br>
				
				<label>Is this a LTL?</label>
				<span><?php echo ' ' . $ltl; ?></span><br>
				
				<label>Is this a DirectPay Load?</label>
				<span><?php echo ' ' . $directPay; ?></span><br>
				
				<label>Is there a Differing Amount?</label>
				<span><?php echo ' ' . $isDifferentAmount; ?></span><br>
				
				<label>Comments:</label>
				<span><?php echo ' ' . $comments; ?></span><br>
				
			</fieldset>
			
			<form action="." method="post">
				<input type="hidden" name="action" value="insert_pro"/>
				
				<input type="submit" value="Submit" class=bottomButton />
				<input type="button" value="Back" onClick="history.go(-1);return true;" class=bottomButton/><br>
			</form> 
			<br>
		</div>
		
<?php include '../../view/footer.php'; ?>