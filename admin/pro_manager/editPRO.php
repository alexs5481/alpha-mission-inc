<?php include '../../view/frontPageHeader.php'; ?>
	<script>
	  	$(function() {
	    		$( "#skills" ).autocomplete({
	      		source: '../../model/searchCompanies.php'
	    		});
	  	});
	  	
        </script>
	<div id='content'>
		<form action="." method="post">
		
			<!---<a href="./?action=">Add/Change Stops</a>
			<br><br>---->
			<fieldset>
				<input type="hidden" name="action" value="edit"/>
				<input type='hidden' name='pro' value='<?php echo $load['proNumber'];?>'/>
				<input type='hidden' name='oldDate' value='<?php echo $load['postedDate'];?>'/>
				
				
				<legend>Edit PRO Form</legend>
				
				<label>PRO#: </label> <?php echo $load['proNumber'];?>
				
				<br>
				<label for="skills">Companies: </label>
				<input id="skills" name='companyName' value='<?php echo $company['companyName'];?>'>
				<br>
				<label>Truck Number: </label>
				<select name="trNumber" value = '<?php echo $load['truckNumber'];?>'>
					<?php foreach ($drivers as $driver) :?>
							<option <?php if($load['truckNumber'] == $driver['truckNumber']): ?> selected <?php endif;?>>
							<?php echo $driver['truckNumber'];?></option>
					<?php endforeach; ?>
				</select>
				<br>
				<label>Load Amount: </label>
				<input type="text" name="amount" class=textbox value='<?php echo $load['loadAmount'];?>' />
				<br>
				<label>Driver Pay: </label>
				<input type="text" name="driverPay" class=textbox value='<?php echo $load['driverPay'];?>' />
				<br>
				<label>Brokers Name</label>
				<input type="text" name="broker" class="textbox" value='<?php echo $load['broker'];?>'>
				<br>
				<label>Broker's Rating</label>
				<select name="brokersRating" >
				
					<?php if($brokersRating == Null):?> 
					<option disabled selected> - </option>
					<?php endif; ?> 
					
					<option value="A" <?php if($brokersRating == "A"): ?>selected<?php endif;?>>A</option>
					<option value="B" <?php if($brokersRating == "B"): ?>selected<?php endif;?>>B</option>
					<option value="C" <?php if($brokersRating == "C"): ?>selected<?php endif;?>>C</option>
					<option value="D" <?php if($brokersRating == "D"): ?>selected<?php endif;?>>D</option>
					<option value="F" <?php if($brokersRating == "F"): ?>selected<?php endif;?>>F</option>
					<option value="I" <?php if($brokersRating == "I"): ?>selected<?php endif;?>>I</option>
					<option value="N" <?php if($brokersRating == "N"): ?>selected<?php endif;?>>N</option>
				</select>
				<br><br>
				<fieldset>
					<legend>Check all that apply</legend>
					<br>
					<input type="checkbox" name="tonu" class='checkboxes' <?php if($load['tonu']):?>checked<?php endif;?>/>
					Truck Order Not Used
					<br>
					<input type="checkbox" name="ltl" class='checkboxes' <?php if($load['LTL']):?>checked<?php endif;?>/>
					LTL Load
					<br>
					<input type="checkbox" name="date" class='checkboxes'/>
					Change Date to Today
					<br>
					<input type="checkbox" name="isDirectPay" class='checkboxes' <?php if($load['isDirectPay']):?>checked<?php endif;?>/>
					Direct Pay
					<br> 
					
				</fieldset>
				<p>Comments:</p>
				<textarea name="comments" rows="4" cols="50"><?php echo $load['comments'];?></textarea>
				
			</fieldset>
			<input type="submit" value="Submit" />
			<input type="button" value="Back" onClick="history.go(-1);return true;" class=bottomButton/><br>
		</form>
	</div>
<?php include '../../view/footer.php'; ?>