<?php include '../../view/frontPageHeader.php'; ?>
	
	<div id='content'>
		<form action="." method="post">
			<fieldset>
				
				<input type="hidden" name="action" value="display_new_add"/>
				<legend>New PRO Form</legend>
				
				<label>PRO#: </label> <?php echo $proNumber;?>
				<br>
				
				<label>Companies: </label>
				<input id="companyName" name='companyName' value='<?php echo $company['companyName'];?>'/>
				<?php if($$company['companyName'] == NULL) echo "<p id='companyName*' style='color:red;'>*</p>";?>
				<br>
				<label>Truck Number: </label>
				<select id="trNumber" name="trNumber">
					<?php foreach ($trucks as $truck):?>
							<?php if($truck['id'] == $trNumber): ?>
							<option selected><?php echo $truck['id']; ?></option>
							<?php else:?>
							<option><?php echo $truck['id']; ?></option>
							<?php endif;?>
					<?php endforeach;?>
				</select>
				<?php /*if($trNumber == "0000") echo "<p id='brokersRating*' style='color:red;'>*</p>";*/?>
				<br>
				<label>Load Amount: </label>
				<input id="name" type="text" name="amount" onchange="completed('name')" class=textbox value='<?php echo $amount;?>'/> 
				<?php if($amount == NULL) echo "<p id='name*' style='color:red;'>*</p>";?>
				<br>
				<label>Driver Pay: </label>
				<input id="driverPay" type="text" name="driverPay" onchange="completed('driverPay')" class=textbox value='<?php echo $driverPay;?>'/>
				<?php if($driverPay == NULL) echo "<p id='driverPay*' style='color:red;'>*</p>";?>
				<br>
				<label>Broker's Name</label>
				<input id="broker" type="text" name="broker" class="textbox" onchange="completed('broker')" value='<?php echo $broker;?>'>
				<?php if($broker == NULL) echo "<p id='broker*' style='color:red;'>*</p>";?>
				<br>
				<label>Broker's Rating</label>
				<select id="brokersRating" name="brokersRating" onchange="completed('brokersRating')" >
					<?php if($brokersRating == Null):?> 
					<option disabled selected> - </option>
					<?php endif; ?> 
					
					<?php if($brokersRating == "A")?>
					<option value="A" <?php if($brokersRating == "A"): ?>selected<?php endif;?>>A</option>
					<option value="B" <?php if($brokersRating == "B"): ?>selected<?php endif;?>>B</option>
					<option value="C" <?php if($brokersRating == "C"): ?>selected<?php endif;?>>C</option>
					<option value="D" <?php if($brokersRating == "D"): ?>selected<?php endif;?>>D</option>
					<option value="F" <?php if($brokersRating == "F"): ?>selected<?php endif;?>>F</option>
					<option value="I" <?php if($brokersRating == "I"): ?>selected<?php endif;?>>I</option>
					<option value="N" <?php if($brokersRating == "N"): ?>selected<?php endif;?>>N</option>
				</select>
				<?php if($brokersRating == NULL) echo "<p id='brokersRating*' style='color:red;'>*</p>";?>
				<br><br>
				<fieldset>
					<legend>Check all that apply</legend>
					<br>
					<input type="checkbox" name="tonu" class=checkboxes <?php if($tonu):?>checked<?php endif;?>/>
					Truck Order Not Used
					<br>
					<input type="checkbox" name="ltl" class=checkboxes <?php if($ltl):?>checked<?php endif;?>/>
					LTL Load
					<br>
					<input type="checkbox" name="directPay" class=checkboxes <?php if($directPay):?>checked<?php endif;?>/>
					Direct Pay
					<br>
				</fieldset>
				<label>Comments:</label>
				<textarea name="comments" rows="4" cols="50" ><?php echo $comments;?></textarea>
				
			</fieldset>
			<input type="submit" value="Submit" />
			<input type="button" value="Back" onClick="history.go(-1);return true;" class=bottomButton/><br>
		</form>
	</div>
<?php include '../../view/footer.php'; ?>
<script>
	function completed(id){
		var x = document.getElementById(id);
		var y = document.getElementById(id+"*");

		if(x.value != ""){
			y.innerHTML = "";
		} else y.innerHTML = "*";
	}
	$(function() {
	    		$( "#companyName" ).autocomplete({
	      		source: '../../model/searchCompanies.php'
	    		});
	 });

</script>