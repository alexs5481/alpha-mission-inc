<?php include '../../view/frontPageHeader.php'; ?>
<script>
	$(function() {
			$( "#company" ).autocomplete({
			source: '../../model/searchCompanies.php'
			});
	});
	</script>
	<script>
	$(function() {
			$( "#pro" ).autocomplete({
			source: '../../model/searchPros.php'
			});
	});
	</script>
	
<div id="content">
	<h1>Accounting PRO Information</h1>

	<form action='.' method='post'>
		<input type='hidden' name='action' value='filter'>
		<label>Needs updated info</label>
		<input id="notUpdated" name="notUpdated" type="checkbox">
		<br>
		<label>Has Inconsistencies</label>
		<input id="inconsistent" name="inconsistent" type="checkbox">
		<br>
		<input type="submit" value="Submit" />
		<br>
		
	</form>
		
		<table border="2">
			<tr>
				<th>PRO#</th>
				<th>Company</th>
				<th>Truck#</th>
				<th>Amount to be Paid</th>
				<th>Paid Amount</th>
				<th>Posted Date</th> 
				<th>Invoice Date</th> 
				<th>Sent to RTS</th>
				<th>TONU?</th>
				<th>Has Incons</th>
				<th>&nbsp;</th>
				
			</tr>
			<?php foreach($loads as $load): ?>
				<tr>
					<td><?php echo $load['proNumber'];?></td>
					<?php $companyID = $load['companyID'];
						  $companyName = get_companyName($companyID); ?>
					<td><?php echo $companyName[0];?></td>
					<td><?php echo $load['truckNumber'];?></td>
					<td><?php echo "$" . $load['loadAmount']; ?></td>
					<td><?php if(!empty($load['paidAmount']))echo "$" . $load['paidAmount']; ?></td>
					<td><?php echo $load['postedDate'];?></td> 
					<td><?php echo $load['invoiceDate'];?></td>
					
					<?php if($load['sentToRTS'] == 0) $sent= "No";
						else $sent= "Yes"; ?>
					<td><?php echo $sent;?></td>
					<?php if($load['tonu'] == 0) $tonu = "No";
						else $tonu = "Yes"; ?>
					<td><?php echo $tonu;?></td>

					<?php if($load['hasInconsistencies'] == 0) $hasIncons = "No";
						else $hasIncons = "Yes"; ?>
					<td><?php echo $hasIncons;?></td>
					
					<td><form action="." method="post">
						<input type="hidden" name="action"
							   value="edit_load">
						<input type="hidden" name="proNumber"
							   value="<?php echo $load['proNumber']; ?>">
						<input type="submit" value="Edit">
					</form></td>
				</tr>
		
			<?php endforeach; ?>
		</table>
	</div>


<?php include '../../view/footer.php'; ?>