<?php 
ob_start();
session_start();

require('../../model/database.php');
require('../../model/states_db.php');
require('../../model/company_db.php');
require('../../model/driver_db.php');
require('../../model/pro_db.php');
require('../../model/brokerRating_db.php');
require('../../model/trucks_db.php');


if(!$_SESSION['user']['isAdmin'])
{
	header('Location: ../user');
}

$user = $_SESSION['user'];

$action = filter_input(INPUT_POST, 'action');
if ($action === NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action === NULL) {
        $action = 'accounting'; 
    }
}


if($action == 'accounting'){
	$companies = get_companies();
	$trucks = getTrucks();
	$loads = get_loads_ASC();
	include('./accounting.php');
}

if($action == 'filter'){

	if(isset($_POST['inconsistent'])) $hasInconsistencies = true;
	if(isset($_POST['notUpdated'])) $notUpdated = true;

	if(!empty($hasInconsistencies)){
		$loads = get_inconsistent_loads();
	} else if(!empty($notUpdated)){
		$loads = get_not_updated_loads();
	} else {
		$loads = get_loads_ASC();
	}


	include('./accounting.php');

}


ob_end_flush();
?>