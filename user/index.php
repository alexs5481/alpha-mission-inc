<?php
ob_start();
session_start();

require('../model/database.php');
require('../model/company_db.php'); 
require('../model/users_db.php');
require('../model/pro_db.php');
require('../model/driver_db.php');
require('../model/comments_db.php');

if(empty($user) && (!empty($_SESSION['user']))){
	$user = $_SESSION['user'];
}

$action = filter_input(INPUT_POST, 'action');
if ($action === NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action === NULL) {
        $action = 'home'; 
    }
}

if ($action == 'home')
{	
    	include('./home.php');
	
} 
if($action == 'view_pro')
{	

	$drivers = get_drivers();
	$loads = get_loads();
	include('./proList.php');	
} 
if ($action == 'filter')
{
	$drivers = get_drivers();
	$pro= filter_input(INPUT_POST, 'pro');
	$companyName = filter_input(INPUT_POST, 'company');
	$company = get_company_by_name($companyName);
	$companyID = $company['companyID'];
	$trNumber = filter_input(INPUT_POST, 'trNumber');
	
	if($pro != NULL)
		$loads = get_load($pro);
	else if($company != NULL)
	{	
		$loads = get_load_by_company($companyID);
	} else if($trNumber != 0000){
		$loads= get_load_by_truck($trNumber);
	} else{
		$loads = get_loads();
	}
	include('./proList.php');
}
if($action == 'add_comment'){
	include('./comments.php');
}
if($action == 'comments'){
	$comment = filter_input(INPUT_POST, 'comments');
	add_comment($comment);
	header('Location: ../user');
}

ob_end_flush();
?>