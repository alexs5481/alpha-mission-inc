<?php include '../view/frontPageHeader.php'; ?>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script>
	  	$(function() {
	    		$( "#company" ).autocomplete({
	      		source: '../model/searchCompanies.php'
	    		});
	  	});
        </script>
        <script>
	  	$(function() {
	    		$( "#pro" ).autocomplete({
	      		source: '../model/searchPros.php'
	    		});
	  	});
        </script>
        
	<div id="content">
		<h1>Admin PRO Information</h1>
		<form action='.' method='post'>
			<input type='hidden' name='action' value='filter'>
			

			<label for="pro">PROs: </label>
			<input id="pro" name='pro'>
			<br>
			<!----
			<label for="company">Companies: </label>
			<input id="company" name='company'>
			<br>---->
		       
			<label>Truck Number: </label>
			<select name="trNumber">
				<?php foreach ($drivers as $driver) :?>
						<option><?php echo $driver['truckNumber']; ?></option>
				<?php endforeach; ?>
			</select>
			<br>
			<input type="submit" value="Submit" />
			<br>
			
	        </form>
	        <table border="2">
	            <tr>
	                <th>PRO#</th>
	                <th>Company</th>
	                <th>Truck#</th>
	                <th>Pay</th>
	                <th>Broker's Name</th>
	                <th>Posted Date</th>
	                <th>TONU?</th>
	                <th>Comments</th>
					
	            </tr>
	           <?php foreach ($loads as $load) :?>
				<tr>
					<td><?php echo $load['proNumber'];?></td>
					<?php $companyID = $load['companyID'];
					      $companyName = get_companyName($companyID); ?>
					<td><?php echo $companyName[0];?></td>
					<td><?php echo $load['truckNumber'];?></td>
					<td><?php echo "$" . $load['driverPay']; ?></td>
					<td><?php echo $load['broker'];?></td>
					<td><?php echo $load['postedDate'];?></td>
					<?php if($load['tonu'] == 0) $tonu = "No";
						else $tonu = "Yes"; ?>
					<td><?php echo $tonu;?></td>
					<td><?php echo $load['comments'];?></td>
				</tr>
				
		  <?php endforeach; ?>
	        </table>
    	</div>
	
<?php include '../view/footer.php'; ?>