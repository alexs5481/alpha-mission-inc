<!DOCTYPE html>
<html>
<head>
	<title>Alpha Mission PRO Board</title>
	<link rel="stylesheet" type="text/css" href="/alpha/main.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	</head>
<body style='width:1400px; margin: auto;'>
	<header id="content">
		<h1>Alpha Mission PRO Board</h1>
		<nav>
			<ul>
				<li id="navb"><a href="/alpha/login/?action=home">Home</a></li>
				
				<?php if(isset($_SESSION['user']['isAdmin'])) :?>
				<li id="navb"><a href="/alpha/admin/pro_manager">ProList</a></li>
				<li id="navb"><a href="/alpha/admin/company_manager">Company List</a></li>
				<?php else:?>
				<li id="navb"><a href="/alpha/user/?action=view_pro">ProList</a></li>
				<?php endif;?>
				<li id="navb"><a href="/alpha/login/?action=logout">Logout</a></li>
			</ul>
		</nav>
	</header>
	<div class="ui-widget">