<?php
ob_start();
$lifetime = 60*60*3;
session_set_cookie_params($lifetime, '/');
session_start();

$action = filter_input(INPUT_POST, 'action');
if ($action === NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action === NULL) {
        $action = 'login_page'; 
    }
}

if ($action == 'login_page'){
	
    header('Location: ./login');
	
}
ob_end_flush();
?>