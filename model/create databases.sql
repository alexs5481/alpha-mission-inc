DROP DATABASE IF EXISTS proBoard;
CREATE DATABASE proBoard;
USE proBoard;

CREATE TABLE drivers
(
	truckNumber 	INT			NOT NULL	UNIQUE,
	firstName		VARCHAR(20)	NOT NULL,
	lastName		VARCHAR(20)	NOT NULL,
	birthday		DATE,	
	isActive		BOOLEAN,
	PRIMARY KEY (truckNumber)
);
CREATE TABLE loads
(
	proNumber		INT 		NOT NULL 	UNIQUE 	AUTO_INCREMENT,
	companyID		INT			NOT NULL,
	truckNumber		INT,
	loadAmount		FLOAT		NOT NULL,
	driverPay		FLOAT,
	invoiceDate		DATE,
	payedDate		DATE,
	tonu 			BOOLEAN,
	underPaid		INT,
	amIncome		INT,
	isEmpty 		BOOLEAN,
	comments		VARCHAR(120),
	PRIMARY KEY (proNumber)
);
CREATE TABLE companies
(
	companyID		INT			NOT NULL	UNIQUE	AUTO_INCREMENT,
	companyName		VARCHAR(50)	NOT NULL,
	stateName		VARCHAR(20)	NOT NULL,
	city			VARCHAR(50) NOT NULL,
	mc				INT,	
	dot				INT,
	isActive		BOOLEAN,
	PRIMARY KEY (companyID)
);

CREATE TABLE states
(
	stateCode	 	char(2) 	NOT NULL,
	stateName		varchar(20)	NOT NULL,
	PRIMARY KEY (stateCode)
);

CREATE TABLE users
(
	userName 		varchar(20)	NOT NULL 	UNIQUE,
	password		varchar(20)	NOT NULL,
	isAdmin			boolean		NOT NULL,
	PRIMARY KEY (userName)
);

INSERT INTO users VALUES
('admin', 'password', 1),
('guest', 'password', 0);

INSERT INTO companies VALUES
('01', 'Total Quality Logistics', 'OH', 'Akron', '12345678', '1234567891', '01');

INSERT INTO drivers VALUES
('58', 'Vasiliy', 'Lashtur', '1000-10-10', '1');

INSERT INTO states VALUES 
('AL','Alabama'),
('AK','Alaska'),
('AZ','Arizona'),
('AR','Arkansas'),
('CA','California'),
('CO','Colorado'),
('CT','Connecticut'),
('DE','Delaware'),
('DC','District of Columbia'),
('FL','Florida'),
('GA','Georgia'),
('HI','Hawaii'),
('ID','Idaho'),
('IL','Illinois'),
('IN','Indiana'),
('IA','Iowa'),
('KS','Kansas'),
('KY','Kentucky'),
('LA','Louisiana'),
('ME','Maine'),
('MD','Maryland'),
('MA','Massachusetts'),
('MI','Michigan'),
('MN','Minnesota'),
('MS','Mississippi'),
('MO','Missouri'),
('MT','Montana'),
('NE','Nebraska'),
('NV','Nevada'),
('NH','New Hampshire'),
('NJ','New Jersey'),
('NM','New Mexico'),
('NY','New York'),
('NC','North Carolina'),
('ND','North Dakota'),
('OH','Ohio'),
('OK','Oklahoma'),
('OR','Oregon'),
('PA','Pennsylvania'),
('RI','Rhode Island'),
('SC','South Carolina'),
('SD','South Dakota'),
('TN','Tennessee'),
('TX','Texas'),
('UT','Utah'),
('VT','Vermont'),
('VA','Virginia'),
('WA','Washington'),
('WV','West Virginia'),
('WI','Wisconsin'),
('WY','Wyoming');

GRANT SELECT
ON *
TO office@localhost
IDENTIFIED BY 'office';

GRANT SELECT, INSERT, UPDATE, DELETE
ON * 
TO alphamissioninc@localhost
IDENTIFIED BY 'alphamissioninc8356';