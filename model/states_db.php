<?php

function get_state($stateCode) {
    global $db;
    $query = 'SELECT * FROM states
              WHERE stateCode = :stateCode';
    $statement = $db->prepare($query);
    $statement->bindValue(":stateCode", $stateCode);
    $statement->execute();
    $product = $statement->fetch();
    $statement->closeCursor();
    return $product;
}
function get_states(){
	global $db;
	$query = 'SELECT * FROM states
                       ORDER BY stateCode';
	$statement = $db->prepare($query);
	$statement->execute();
	$states = $statement->fetchAll();	
	$statement->closeCursor();
	return $states;
}

?>