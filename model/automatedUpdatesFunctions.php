<?php
function update(){
	if (($handle = fopen("dxGv_1.csv", "r")) !== FALSE) {
		
		$invoiceNum = 0;
		$debtorName = 0;
		$paidDate = "";
		$purchaseDate = 0;
		$payment = 0;
		$feeAmount = 0;
		
		$companies = get_companies();
		$loads = get_loads();
		
		$loadInformationIsDifferent = array();

		$count =0;
		
		while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

			
			$count++;
			if($data[1]=="Cash"){
				$paidDate = $data[0];
			}
			
			if($data[0] == '=("Invoice#")'){

				for($i = 1; $i<20; $i++){
					if($data[$i] == '=("Debtor")'){
						$debtorName = $i;
					}
					if($data[$i] == '=("Purchase Date")'){
						$purchaseDate = $i;
					}
					if($data[$i] == '=("Payment")'){
						$payment = $i;
					}
					if($data[$i] == '=("Fee Amt")'){
						$feeAmount = $i;
					}
				}
			}

			if((!empty($data[$payment])) && (!empty($data[$invoiceNum]))) {
				foreach ($loads as $load){
					
					if($data[0]== $load['proNumber']){
						
						//echo nl2br("<p>\nLOAD# " . $data[$invoiceNum] . "\nCompany: " . $data[$debtorName]. "\nPurchase Date: " 
						//. $data[$purchaseDate] . "\nPaid Date: " . $paidDate . "\nPayment: " . $data[$payment] . "\nFee Amount: " . $data[$feeAmount] .  "</p><br>");

						$companyID = $load['companyID'];
						$companyName = '';
						foreach($companies as $company){
							if($companyID == $company['companyID']){
								$companyName = $company['companyName'];
								$break;
							}
						}
						$amountPaid = trimPaymentAmount($data[$payment]);
						
						
						echo "Load# " . $data[$invoiceNum];

						if($data[$debtorName] != $companyName){
							echo " has a different name (" . $data[$debtorName] . "), ";
							//$hasInconsistency = true;
						} 
						if($amountPaid != $load['loadAmount']){
							echo " has a different Amount, ";
							$hasInconsistency = true;
						} 

						if(!empty($hasInconsistency) && $hasInconsistency == true){
							setHasInconsistencies($load['proNumber']);
							echo " has inconsistencies<br>";
							$hasInconsistency = false;
						} else {
							$rtsFee = trimPaymentAmount($data[$feeAmount]);
							updateLoadInfo($load['proNumber'], $amountPaid, $data[$purchaseDate], $paidDate, $rtsFee);
							echo " has been added<br>";
						}

						break;
						
					}
				}
			}
		}
		fclose($handle);
	}
	
	foreach ($loadInformationIsDifferent as $isDifferent){
		echo "<br>The following loads have different load information<br>" . $isDifferent[0];
	}
}

function trimPaymentAmount($payment){
	$trimedNum = trim(str_replace('$', "", $payment));
	$trimedNum = trim(str_replace(',', "", $trimedNum));
	$trimedNum = (float)$trimedNum;
	return $trimedNum;
}

function updateLoadInfo($proNumber, $paidAmount, $invoiceDate, $paidDate, $rtsFee){
	global $db;
	$query = 'UPDATE loads
			SET paidAmount = :paidAmount,
				invoiceDate = :invoiceDate,
				paidDate = :paidDate,
				rtsFee = :rtsFee,
				sentToRTS = 1,
				isPaid = 1
			WHERE proNumber= :proNumber';
	$statement = $db->prepare($query);
	$statement->bindValue(':proNumber', $proNumber);
	$statement->bindValue(':paidAmount', $paidAmount);
	$statement->bindValue(':invoiceDate', $invoiceDate);
	$statement->bindValue(':paidDate', $paidDate);
	$statement->bindValue(':rtsFee', $rtsFee);
	$statement->execute();
	$statement->closeCursor();
}

function setHasInconsistencies($proNumber){
	global $db;
	$query = 'UPDATE loads
			SET hasInconsistencies = 1
			WHERE proNumber= :proNumber';
	$statement = $db->prepare($query);
	$statement->bindValue(':proNumber', $proNumber);
	$statement->execute();
	$statement->closeCursor();
}
?>