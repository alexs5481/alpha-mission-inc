<?php
function addDues($comments, $priority){
	global $db;
    $query = 'INSERT INTO comments
                 (comments, priority)
              VALUES
                 (:comments, :priority)';
    $statement= $db->prepare($query);
    $statement->bindValue(':comments', $comments);
    $statement->bindValue(':priority', $priority);
    $statement->execute();
    $statement->closeCursor();
}
function getDues() {
    global $db;
	$query = 'SELECT * FROM dues';
	$statement= $db->prepare($query);
	$statement->execute();
	$dues = $statement->fetchAll();	
	$statement->closeCursor();
	return $dues;
}
function updateDues($id, $comments, $priority){
	global $db;
	$query = 'UPDATE comments
			SET comments = :comments,
			priority = :priority
			WHERE id = :id';
	$statement = $db->prepare($query);
    	$statement->bindValue(':id', $id);
    	$statement->bindValue(':comments', $comments);
    	$statement->bindValue(':priority', $priority);
    	$statement->execute();
    	$statement->closeCursor();
}

?>
	