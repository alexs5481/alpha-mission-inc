<?php

function getTrucks(){
	global $db;
	$query = 'SELECT * FROM trucks
					   ORDER BY id';
	$statement = $db->prepare($query);
	$statement->execute();
	$trucks = $statement->fetchAll();
	$statement->closeCursor();
	return $trucks;
}

function getActiveTrucks(){
	global $db;
	$query = 'SELECT * FROM trucks
					   WHERE active = 1
					   ORDER BY id';
	$statement = $db->prepare($query);
	$statement->execute();
	$trucks = $statement->fetchAll();
	$statement->closeCursor();
	return $trucks;
}

function getInActiveTrucks(){
	global $db;
	$query = 'SELECT * FROM trucks
					   WHERE active = 0
					   ORDER BY id';
	$statement = $db->prepare($query);
	$statement->execute();
	$trucks = $statement->fetchAll();
	$statement->closeCursor();
	return $trucks;
}

function setTruckInactive($id){
	global $db;
	$query = 'UPDATE trucks
			SET active = 0
			WHERE id = :id';
	$statement = $db->prepare($query);
	$statement->bindValue(':id', $id);
	$statement->execute();
	$statement->closeCursor();
}
function setTruckActive($id){
	global $db;
	$query = 'UPDATE trucks
			SET active = 1
			WHERE id = :id';
	$statement = $db->prepare($query);
	$statement->bindValue(':id', $id);
	$statement->execute();
	$statement->closeCursor();
}
function addTruck($trNumber){
	global $db;
   	$query = 'INSERT INTO trucks
                 (id)
              VALUES
                 (:id)';
	$statement = $db->prepare($query);
	$statement->bindValue(':id', $trNumber);
	$statement->execute();
	$statement->closeCursor();
}
?>
	