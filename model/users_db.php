<?php
function get_user($userName, $password){
	global $db;
	$query = 'SELECT * FROM users
			  WHERE userName = :userName AND password = :password';
	$statement = $db->prepare($query);
	$statement->bindValue(':userName', $userName);
	$statement->bindValue(':password', $password);
	$statement->execute();
	$user = $statement->fetch();
	$statement->closeCursor();
	return $user;
}

function getSalt($userName){
	global $db;
	$query = 'SELECT salt FROM users
			  WHERE userName = :userName';
	$statement = $db->prepare($query);
	$statement->bindValue(':userName', $userName);
	$statement->execute();
	$user = $statement->fetch();
	$statement->closeCursor();
	return $user;
}

function changePassword($userName, $saltedPassword, $iv){
	global $db;
	$query = 'Update users
				SET password = :password,
				salt = :salt
				WHERE userName = :userName';
	$statement = $db->prepare($query);
	$statement->bindValue(':userName', $userName);
	$statement->bindValue(':password', $saltedPassword);
	$statement->bindValue(':salt', $iv);
	$statement->execute();
	$statement->closeCursor();
}
?>
	