<?php
function add_rating($companyID, $date, $rating){
	global $db;
	$query = 'INSERT INTO brokerRatings
                 (companyID, date, rating)
		  VALUES
                 (:companyID, :date, :rating)';
	$statement = $db->prepare($query);
	$statement->bindValue(':companyID', $companyID);
	$statement->bindValue(':date', $date);
	$statement->bindValue(':rating', $rating);
	$statement->execute();
	$statement->closeCursor();
}
function add_brokerRating($companyID, $brokersRating, $postedDate){
	global $db;
   	$query = 'INSERT INTO brokerRatings
                 (companyID, brokersRating, postedDate)
              VALUES
                 (:companyID, :brokersRating, :postedDate)';
	$statement = $db->prepare($query);
	$statement->bindValue(':companyID', $companyID);
	$statement->bindValue(':brokersRating', $brokersRating);
	$statement->bindValue(':postedDate', $postedDate);
	$statement->execute();
	$statement->closeCursor();
}
/*
function add_company($city, $state, $companyName, $mc, $dot){
	global $db;
    $query = 'INSERT INTO companies
                 (city, stateName, companyName, mc, dot)
					VALUES
                 (:city, :stateName, :companyName, :mc, :dot)';
    $statement = $db->prepare($query);
    $statement->bindValue(':city', $city);
    $statement->bindValue(':stateName', $state);
    $statement->bindValue(':companyName', $companyName);
    $statement->bindValue(':mc', $mc);
    $statement->bindValue(':dot', $dot);
    $statement->execute();
    $statement->closeCursor();
}
function get_companies() {
    global $db;
	$query = 'SELECT * FROM companies
                       ORDER BY companyName';
	$statement = $db->prepare($query);
	$statement->execute();
	$companies = $statement->fetchAll();	
	$statement->closeCursor();
	return $companies;
}

function update_company($companyID, $name, $mc, $dot, $city, $state){
	global $db;
	$query = 'UPDATE companies
		  SET companyName = :name,
		  mc = :mc,
		  dot = :dot,
		  city = :city,
		  stateName = :state
		  WHERE companyID = :companyID';
	$statement = $db->prepare($query);
    	$statement->bindValue(':name', $name);
    	$statement->bindValue(':mc', $mc);
    	$statement->bindValue(':dot', $dot);
    	$statement->bindValue(':city', $city);
    	$statement->bindValue(':state', $state);
    	$statement->bindValue(':companyID', $companyID);
    	$statement->execute();
    	$statement->closeCursor();
}*/
?>
	