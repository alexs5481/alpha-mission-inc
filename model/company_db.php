<?php
function add_company($city, $state, $companyName, $mc, $dot, $brokerRating, $comments, $blacklist, $blackListReason){
	global $db;
    $query = 'INSERT INTO companies
                 (city, stateName, companyName, mc, dot, latestRating, comments, blacklist, blackListReason)
					VALUES
                 (:city, :stateName, :companyName, :mc, :dot, :latestRating, :comments, :blacklist, :blackListReason)';
    $statement = $db->prepare($query);
    $statement->bindValue(':city', $city);
    $statement->bindValue(':stateName', $state);
    $statement->bindValue(':companyName', $companyName);
    $statement->bindValue(':mc', $mc);
    $statement->bindValue(':dot', $dot);
    $statement->bindValue(':latestRating', $brokerRating);
    $statement->bindValue(':comments', $comments);
    $statement->bindValue(':blacklist', $blacklist);
    $statement->bindValue(':blackListReason', $blackListReason);
    $statement->execute();
    $statement->closeCursor();
}
function get_companies() {
    global $db;
	$query = 'SELECT * FROM companies
                       ORDER BY companyName';
	$statement = $db->prepare($query);
	$statement->execute();
	$companies = $statement->fetchAll();	
	$statement->closeCursor();
	return $companies;
}
function get_company_by_id($companyID){
	global $db;
	$query = 'SELECT * FROM companies
			  WHERE companyID = :companyID';
	$statement = $db->prepare($query);
	$statement->bindValue(':companyID', $companyID);
	$statement->execute();
	$company = $statement->fetch();
	$statement -> closeCursor();
	return $company;
}
function get_company_by_name($name){
	global $db;
	$query = 'SELECT * FROM companies
			  WHERE companyName = :name';
	$statement = $db->prepare($query);
	$statement->bindValue(':name', $name);
	$statement->execute();
	$company = $statement->fetch();
	$statement -> closeCursor();
	return $company;
}
function get_companies_by_name($name){
	global $db;
	$query = 'SELECT * FROM companies
			  WHERE companyName = :name';
	$statement = $db->prepare($query);
	$statement->bindValue(':name', $name);
	$statement->execute();
	$company = $statement->fetchAll();
	$statement -> closeCursor();
	return $company;
}
function get_companies_by_like_name($name){
	global $db;
	$query = 'SELECT * FROM companies
			  WHERE companyName LIKE  "%' . $name . '%"
			  ORDER by companyName ASC';
	$statement = $db->prepare($query);
	$statement->bindValue(':name', $name);
	$statement->execute();
	$company = $statement->fetchAll();
	$statement -> closeCursor();
	return $company;
}
function get_companyName($companyID){
	global $db;
	$query = 'SELECT companyName FROM companies
			  WHERE companyID= :companyID';
	$statement = $db->prepare($query);
	$statement->bindValue(':companyID', $companyID);
	$statement->execute();
	$companyName = $statement->fetch();
	$statement -> closeCursor();
	return $companyName;
}
function get_black_list_companies(){
	global $db;
	$query = 'SELECT * FROM companies
		  WHERE blackList = 1
		  ORDER BY companyName';
	$statement = $db->prepare($query);
	$statement->bindValue(':companyID', $companyID);
	$statement->execute();
	$company = $statement->fetchAll();
	$statement -> closeCursor();
	return $company;	  
}
function set_preferred($companyID){
	global $db;
	$query = 'UPDATE companies
		  SET preferredList = 1,
		  blackList = 0
		  WHERE companyID = :companyID';
	$statement = $db->prepare($query);
	$statement->bindValue(':companyID', $companyID);
	$statement->execute();
	$statement -> closeCursor();
}
function set_blackList($companyID){
	global $db;
	$query = 'UPDATE companies
		  SET blackList = 1,
		  preferredList = 0
		  WHERE companyID = :companyID';
	$statement = $db->prepare($query);
	$statement->bindValue(':companyID', $companyID);
	$statement->execute();
	$statement -> closeCursor();
}
function clear_lists($companyID){
	global $db;
	$query = 'UPDATE companies
		  SET blackList = 0,
		  preferredList = 0
		  WHERE companyID = :companyID';
	$statement = $db->prepare($query);
	$statement->bindValue(':companyID', $companyID);
	$statement->execute();
	$statement -> closeCursor();
}
function update_company($companyID, $name, $mc, $dot, $city, $state, $blackListReason, $comments, $latestRating){
	global $db;
	$query = 'UPDATE companies
		  SET companyName = :name,
		  mc = :mc,
		  dot = :dot,
		  city = :city,
		  stateName = :state,
		  blackListReason = :blackListReason,
		  comments = :comments,
		  latestRating = :latestRating
		  WHERE companyID = :companyID';
	$statement = $db->prepare($query);
	$statement->bindValue(':name', $name);
	$statement->bindValue(':mc', $mc);
	$statement->bindValue(':dot', $dot);
	$statement->bindValue(':city', $city);
	$statement->bindValue(':state', $state);
	$statement->bindValue(':companyID', $companyID);
	$statement->bindValue(':blackListReason', $blackListReason);
	$statement->bindValue(':comments', $comments);
	$statement->bindValue(':latestRating', $latestRating);
	$statement->execute();
	$statement->closeCursor();
}
function updateCompanyRating($companyID, $brokersRating){
		global $db;
	$query = 'UPDATE companies
		  SET latestRating = :latestRating
		  WHERE companyID = :companyID';
	$statement = $db->prepare($query);
	$statement->bindValue(':latestRating', $brokersRating);
	$statement->bindValue(':companyID', $companyID);
	$statement->execute();
	$statement->closeCursor();
}
?>
	