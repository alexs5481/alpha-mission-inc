<?php
function add_driver($firstName, $lastName, $truckNumber, $birthday, $dateAdded, $phoneNum){
	global $db;
    $query = 'INSERT INTO drivers
                 (truckNumber,firstName, lastName, birthday, dateAdded, phoneNum)
              VALUES
                 (:truckNumber, :firstName, :lastName, :birthday, :dateAdded, :phoneNum)';
    $statement= $db->prepare($query);
    $statement->bindValue(':firstName', $firstName);
    $statement->bindValue(':lastName', $lastName);
    $statement->bindValue(':truckNumber', $truckNumber);
    $statement->bindValue(':birthday', $birthday);
    $statement->bindValue(':dateAdded', $dateAdded);
    $statement->bindValue(':phoneNum', $phoneNum);
    $statement->execute();
    $statement->closeCursor();
}
function get_drivers() {
    global $db;
	$query = 'SELECT * FROM drivers
                       ORDER BY truckNumber';
	$statement= $db->prepare($query);
	$statement->execute();
	$drivers = $statement->fetchAll();	
	$statement->closeCursor();
	return $drivers;
}
function getActiveDrivers() {
    global $db;
	$query = 'SELECT * FROM drivers
					   WHERE isActive = 1
                       ORDER BY truckNumber';
	$statement= $db->prepare($query);
	$statement->execute();
	$drivers = $statement->fetchAll();	
	$statement->closeCursor();
	return $drivers;
}
function getTerminatedDrivers() {
    global $db;
	$query = 'SELECT * FROM drivers
					   WHERE isActive = 0
                       ORDER BY truckNumber';
	$statement= $db->prepare($query);
	$statement->execute();
	$drivers = $statement->fetchAll();	
	$statement->closeCursor();
	return $drivers;
}
function get_driver_by_id($driverID){
	global $db;
	$query = 'SELECT * FROM drivers
		  WHERE id = :id';
	$statement= $db->prepare($query);
	$statement->bindValue(':id', $driverID);
	$statement->execute();
	$driver = $statement->fetch();
	$statement->closeCursor();
	return $driver;
}
function terminate_driver($driverID, $date){
	global $db;
	$query = 'UPDATE drivers
			SET terminationDate = :date,
			isActive = 0
			WHERE id = :id';
	$statement = $db->prepare($query);
   	$statement->bindValue(':date', $date);
   	$statement->bindValue(':id', $driverID);
    	$statement->execute();
    	$statement->closeCursor();
}
function reinstate_driver($driverID){
	global $db;
	$query = 'UPDATE drivers
			SET isActive = 1
			WHERE id = :id';
	$statement = $db->prepare($query);
   	$statement->bindValue(':id', $driverID);
    	$statement->execute();
    	$statement->closeCursor();
}
function update_driver($driverID, $truckNumber, $firstName, $lastName, $birthday, $phoneNum){
	global $db;
	$query = 'UPDATE drivers
			SET truckNumber = :truckNumber,
			firstName = :firstName,
			lastName = :lastName,
			birthday = :birthday,
			phoneNum = :phoneNum
			WHERE id = :id';
	$statement = $db->prepare($query);
    	$statement->bindValue(':truckNumber', $truckNumber);
    	$statement->bindValue(':firstName', $firstName);
    	$statement->bindValue(':lastName', $lastName);
    	$statement->bindValue(':birthday', $birthday);
    	$statement->bindValue(':id', $driverID);
    	$statement->bindValue(':phoneNum', $phoneNum);
    	$statement->execute();
    	$statement->closeCursor();
}

?>
	