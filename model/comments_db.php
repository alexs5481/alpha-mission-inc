<?php
function add_comment($comments, $priority){
	global $db;
    $query = 'INSERT INTO comments
                 (comments, priority)
              VALUES
                 (:comments, :priority)';
    $statement= $db->prepare($query);
    $statement->bindValue(':comments', $comments);
    $statement->bindValue(':priority', $priority);
    $statement->execute();
    $statement->closeCursor();
}
function getNotFinishedComments() {
    global $db;
	$query = 'SELECT * FROM comments
					   WHERE done = 0
                       ORDER BY priority DESC';
	$statement= $db->prepare($query);
	$statement->execute();
	$comments = $statement->fetchAll();	
	$statement->closeCursor();
	return $comments;
}
function getFinishedComments() {
    global $db;
	$query = 'SELECT * FROM comments
					   WHERE done = 1
                       ORDER BY id';
	$statement= $db->prepare($query);
	$statement->execute();
	$comments = $statement->fetchAll();	
	$statement->closeCursor();
	return $comments;
}
function commentCompleted($id){
	global $db;
	$query = 'UPDATE comments
			SET done = 1
			WHERE id = :id';
	$statement = $db->prepare($query);
    	$statement->bindValue(':id', $id);
    	$statement->execute();
    	$statement->closeCursor();
}
function commentNotCompleted($id){
	global $db;
	$query = 'UPDATE comments
			SET done = 0
			WHERE id = :id';
	$statement = $db->prepare($query);
    	$statement->bindValue(':id', $id);
    	$statement->execute();
    	$statement->closeCursor();
}
function getComment($id) {
    global $db;
	$query = 'SELECT * FROM comments
					   WHERE id = :id';
	$statement= $db->prepare($query);
	$statement->bindValue(':id', $id);
	$statement->execute();
	$comment = $statement->fetch();	
	$statement->closeCursor();
	return $comment;
}
function updateComment($id, $comments, $priority){
	global $db;
	$query = 'UPDATE comments
			SET comments = :comments,
			priority = :priority
			WHERE id = :id';
	$statement = $db->prepare($query);
    	$statement->bindValue(':id', $id);
    	$statement->bindValue(':comments', $comments);
    	$statement->bindValue(':priority', $priority);
    	$statement->execute();
    	$statement->closeCursor();
}

?>
	