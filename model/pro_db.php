<?php
function add_pro($pro, $companyID, $loadAmount, $driverPay, $isDifferentAmount, $truckNumber, $broker, $brokersRating, $postedDate, $comments, $tonu, $directPay, $ltl){
	global $db;
   	$query = 'INSERT INTO loads
                 (companyID, loadAmount, driverPay, isDifferentAmount, broker, brokersRating, truckNumber, postedDate, comments, tonu, isDirectPay, ltl)
              VALUES
                 (:companyID, :loadAmount, :driverPay, :isDifferentAmount, :broker, :brokersRating, :truckNumber, :postedDate, :comments, :tonu, :directPay, :ltl)';
	$statement = $db->prepare($query);
	$statement->bindValue(':companyID', $companyID);
	$statement->bindValue(':loadAmount', $loadAmount);
	$statement->bindValue(':driverPay', $driverPay);
	$statement->bindValue(':tonu', $tonu);
	$statement->bindValue(':isDifferentAmount', $isDifferentAmount);
	$statement->bindValue(':broker', $broker);
	$statement->bindValue(':brokersRating', $brokersRating);
	$statement->bindValue(':truckNumber', $truckNumber);
	$statement->bindValue(':postedDate', $postedDate );
	$statement->bindValue(':comments', $comments);
	$statement->bindValue(':directPay', $directPay);
	$statement->bindValue(':ltl', $ltl);
	$statement->execute();
	$statement->closeCursor();
}
function get_loads(){
	global $db;
	$query = 'SELECT * FROM loads
		  ORDER BY proNumber DESC';
	$statement = $db->prepare($query);
	$statement->execute();
	$loads = $statement->fetchAll();
	$statement->closeCursor();
	return $loads;
}
function get_loads_ASC(){
	global $db;
	$query = 'SELECT * FROM loads
		  ORDER BY proNumber ASC';
	$statement = $db->prepare($query);
	$statement->execute();
	$loads = $statement->fetchAll();
	$statement->closeCursor();
	return $loads;
}
function get_next_pro(){
	global $db;
	$query = 'SHOW TABLE STATUS LIKE "loads"';
	$statement = $db->prepare($query);
	$statement -> execute();
	$data = $statement->fetch();
	$statement->closeCursor();
	$proNumber = $data['Auto_increment'];
	return $proNumber;
}
function get_load_by_id($proNumber){
	global $db;
	$query = 'SELECT * FROM loads
		  WHERE proNumber = :proNumber';
	$statement = $db->prepare($query);
	$statement->bindValue(':proNumber', $proNumber);
	$statement->execute();
	$load = $statement->fetch();
	$statement->closeCursor();
	return $load;
}
function get_load($proNumber){
	global $db;
	$query = 'SELECT * FROM loads
		  WHERE proNumber = :proNumber';
	$statement = $db->prepare($query);
	$statement->bindValue(':proNumber', $proNumber);
	$statement->execute();
	$load = $statement->fetchAll();
	$statement->closeCursor();
	return $load;
}
function edit_pro($proNumber, $companyID, $amount, $driverPay, $broker, $comments, $tonu, $date, $isDifferentAmount, $trNumber, $ltl, $isDirectPay, $brokersRating){
	global $db;
	$query = 'UPDATE loads
			SET companyID = :companyID,
			loadAmount = :amount,
			driverPay = :driverPay,
			broker = :broker,
			comments = :comments,
			tonu = :tonu,
			ltl = :ltl,
			postedDate = :date,
			isDifferentAmount = :isDifferentAmount,
			truckNumber = :truckNumber,
			isDirectPay = :isDirectPay,
			brokersRating = :brokersRating
			WHERE proNumber= :proNumber';
	$statement = $db->prepare($query);
    	$statement->bindValue(':companyID', $companyID);
    	$statement->bindValue(':proNumber', $proNumber);
    	$statement->bindValue(':amount', $amount);
    	$statement->bindValue(':driverPay', $driverPay);
    	$statement->bindValue(':broker', $broker);
    	$statement->bindValue(':comments', $comments);
    	$statement->bindValue(':tonu', $tonu);
    	$statement->bindValue(':date', $date);
    	$statement->bindValue(':isDifferentAmount', $isDifferentAmount);
		$statement->bindValue(':isDirectPay', $isDirectPay);
    	$statement->bindValue(':truckNumber', $trNumber);
		$statement->bindValue(':brokersRating', $brokersRating);
    	$statement->bindValue(':ltl', $ltl);
    	$statement->execute();
    	$statement->closeCursor();
}
function get_load_by_company($companyID){
	global $db;
	$query = 'SELECT * FROM loads
		  WHERE companyID = :companyID
		  ORDER BY proNumber DESC';
	$statement = $db->prepare($query);
	$statement->bindValue(':companyID', $companyID);
	$statement->execute();
	$loads = $statement->fetchAll();
	$statement->closeCursor();
	return $loads;
}

function get_load_by_truck($truckNumber){
	global $db;
	$query = 'SELECT * FROM loads
		  WHERE truckNumber = :truckNumber
		  ORDER BY proNumber DESC';
	$statement = $db->prepare($query);
	$statement->bindValue(':truckNumber', $truckNumber);
	$statement->execute();
	$loads = $statement->fetchAll();
	$statement->closeCursor();
	return $loads;
}

function get_diffAmount_loads(){
	global $db;
	$query = 'SELECT * FROM loads
		  WHERE isDifferentAmount = 1
		  ORDER BY proNumber DESC';
	$statement = $db->prepare($query);
	$statement->execute();
	$loads = $statement->fetchAll();
	$statement->closeCursor();
	return $loads;
}

function get_inconsistent_loads(){
	global $db;
	$query = 'SELECT * FROM loads
		  WHERE hasInconsistencies = 1
		  ORDER BY proNumber DESC';
	$statement = $db->prepare($query);
	$statement->execute();
	$loads = $statement->fetchAll();
	$statement->closeCursor();
	return $loads;
}

function get_not_updated_loads(){
	global $db;
	$query = 'SELECT * FROM loads
		  WHERE paidAmount IS NULL
		  ORDER BY proNumber DESC';
	$statement = $db->prepare($query);
	$statement->execute();
	$loads = $statement->fetchAll();
	$statement->closeCursor();
	return $loads;
}

?>
	