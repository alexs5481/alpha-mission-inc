<?php
ob_start();
$lifetime = 0;
session_set_cookie_params($lifetime, '/');
session_start();

require('../model/database.php');
require('../model/company_db.php'); 
require('../model/users_db.php');

if(empty($user) && (!empty($_SESSION['user']))){
	$user = $_SESSION['user'];
}

$action = filter_input(INPUT_POST, 'action');

$userName = filter_input(INPUT_POST, 'userName');
$userName = htmlspecialchars($userName);
$userName = strtoupper($userName);
$password = filter_input(INPUT_POST, 'password');
$password = htmlspecialchars($password);
$error = Null;
if ($action === NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action === NULL) {
        $action = 'login_page'; 
    }
}

if ($action == 'login_page'){
	
    include('./login.php');
	
} else if($action == 'login'){
	$salt = getSalt($userName);
	$saltedPassword = $salt['salt'] . $password;
	$encryptedPassword = md5($saltedPassword);
	
	$user = get_user($userName, $encryptedPassword);
	if($user == NULL){
		$error = 'Incorrect Username or Password. Please try Again';
		include('./login.php');
		
	} else {
		$_SESSION['user'] = $user;
		if($user['isAdmin']){
			header('Location: ../admin');
		} else{
			header('Location: /alpha/user');
		}
	}
}else if($action == 'logout'){
	$_SESSION = array();
	
	$name = session_name();
	$expire = strtotime ('-1 year');
	$params = session_get_cookie_params();
	$path = $params['path'];
	$domain = $params['domain'];
	$secure = $params['secure'];
	$httponly = $params['httponly'];
	setcookie($name, '', $expire, $path, $domain, $secure, $httponly);
		
	session_destroy();
	header("Location: ../index.php");
} else if($action == 'home'){
	if(!$user['idAdmin'])
		header('Location: ../admin');
}
ob_end_flush();
?>